import React, { Component } from "react";
import img1 from "./images/MyStore-sale.jpg";
import auth from  './authService';
import http from "./httpService";
class Checkout extends Component{
    state={
        cart:this.props.cart,
        billDetails:{name:"",address1:"",address2:""},
        errors:[],
    };

    handleChange=(e)=>{
        const {currentTarget:input}=e;
        let s1={...this.state};
        s1.billDetails[input.name]=input.value;
        this.setState(s1);
    };

    async post(url,obj){
        let response=await http.post(url, obj);
        console.log(response);
        
    }


    handleSubmit=(e)=>{
        e.preventDefault();
        const user= auth.getUser();
    
        let {cart,billDetails}=this.state;
        let total= cart ? cart.reduce((acc,cur)=>(+cur.quantity)*(+cur.price)+acc ,0) : 0;
        let item=cart ? cart.reduce((acc,cur)=>(+cur.quantity) +acc , 0) : 0;
        let bill={};
        bill.name=billDetails.name;
        bill.address=billDetails.address1;
        bill.city=billDetails.address2;
        bill.totalPrice=total;
        bill.items=item;
        bill.email= user.email;
        let errors=this.validateAll();
        if(this.isValid(errors)){
            
            //this.props.onSubmit(bill);
            this.post("/orders",bill);
            cart={};
            this.props.onSubmit(cart);

            this.props.history.push("/thanks");
            
        }
        
        else{
            let s1={...this.state};
            s1.errors=errors;
            this.setState(s1);
        } 
        
    }

    isValid=(errors)=>{
        let keys=Object.keys(errors);
        let count=keys.reduce((acc,cur)=>errors[cur] ? acc+1 : acc ,0);
        return count===0;
    }

    validateAll=()=>{
        let {name,address1,address2}=this.state.billDetails;
        let errors={};
        
        errors.name=this.validateName(name);
        errors.address1=this.validateAdd1(address1);
        errors.address2=this.validateAdd2(address2);
        
        return errors;
    }


    validateName=(name)=>
    !name ? "Name must be entered"  
        : "";
    
    validateAdd1=(address1)=>
    !address1 ? "Address must be entered" 
        : "";

     validateAdd2=(address2)=>
    !address2 ? "Address must be entered" 
        : "";
    
    render(){
        let cart=this.props.cart;
        console.log(cart);
        let {name,address1,address2}=this.state.billDetails;
        let {errors}=this.state;
        let total=cart.length>0 ? cart.reduce((acc,cur)=>(+cur.quantity)*(+cur.price)+acc ,0) : 0;
        return (
            <div className="container">
                    <div className="row">
                    <div className="col-1"></div>
                    <div className="col-10 ">
                        <img src={img1} style={{width:100 + "%",height:270}}/>
                        
                    </div>
                    <div className="col-1"></div>
                    </div>
                    <div className="row">
                        <div className="col-1"></div>
                        <div className="col-10 border">

                        <h5 className="text-center">Summary of Your Order</h5>
                        <br/>
                        <h6 className="text-center">Your order has {cart.length} items</h6>
                        <div className="row bg-secondary">
                            <div className="col-4 text-center text-dark">Name</div>
                            <div className="col-4 text-center  text-dark">Quantity</div>
                            <div className="col-4 text-center  text-dark">Value</div>
                        </div>
                        {cart.length>=1 ? (
                            <React.Fragment>
                                {cart.map(m=>(
                                    <div className="row border" key={m.name}>
                                        <div className="col-4 text-center">{m.name}</div>
                                        <div className="col-4 text-center">{m.quantity}</div>
                                        <div className="col-4 text-center">{(+m.price)*(+m.quantity)}</div>
                                    </div>
                                    
                                ))}
                                <div className="row border">
                                    <div className="col-4 text-center p-1">Total</div>
                                    <div className="col-4 text-center p-1"></div>
                                    <div className="col-4 text-center p-1">Rs.{total}</div>
                                </div>
                            </React.Fragment>

                        ):("")}

                        <br/>

                        <h4 className="text-center">Delivery Details</h4>
                        <br/>
                        <div className="col-12">
                            {this.showTextBox("Name",name,"name",errors.name,"Enter Name")}
                        </div>
                        <div className="col-12">
                            {this.showTextBox("Address Line1",address1,"address1",errors.address1,"Line 1")}
                        </div>
                        <div className="col-12">
                            {this.showTextBox("",address2,"address2",errors.address2,"Line 2")}
                        </div>
                        <br/>
                        <div className="col">
                            <button className="btn btn-sm btn-primary m-2"
                             onClick={this.handleSubmit}>Submit</button>
                        </div>

                        </div>
                        <div className="col-1"></div>
                    </div>
                    
                    </div>
        );
    }

    showTextBox=(label,value,name,error,placeholder)=>{
       
        return (
            <React.Fragment>
                <div className="form-group">
                    <label className="fw-bold" >{label} </label>
                    <input
                     type="text"
                     className="form-control"
                     id={name}
                     name={name}
                     value={value}
                     //disabled={disabled}
                     placeholder={placeholder}
                     onChange={this.handleChange}
                    />
                    {error ? (
                        <span className="text-danger">{error}</span>
                    ) :(
                        ""
                    )}
                    
                   
                </div>
                </React.Fragment>
         );
    }
}
export default Checkout;