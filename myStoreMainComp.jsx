import React, { Component } from "react";
import {Route,Switch,Redirect} from "react-router-dom";
import MyStoreNav from "./myStoreNav";
import MyStore from "./myStore";
import Cart from "./cart";
import auth from  './authService';
import MyStoreLogin from "./myStoreLogin";
import MyStoreLogout from "./myStoreLogout";
import Checkout from "./checkout";
import ThanksPage from "./thanksPage";
import OrderList from "./orderList";
import ManageProduct from "./manageProduct";
import DeleteProduct from "./deleteProduct";
import EditProduct from "./editProduct";
import AddNewProduct from "./addNewProduct";


class MyStoreMainComp extends Component{
    state={
        cart:[],
        orders:[],
    };

    handleChange=(item)=>{
        let s1={...this.state};
        s1.cart=item;

        this.setState(s1);
        //console.log(s1.cart);
    }
    handleSubmit=(item)=>{
        let s1={...this.state};
        //s1.orders.push(item);
        s1.cart=[];
        this.setState(s1);
    }
    
    
    render(){
        let {cart,orders}=this.state;
        const user= auth.getUser();
       
        return (
            <div className="">
                <MyStoreNav cart={cart} />
                <Switch>


                <Route path="/addProduct" component={AddNewProduct} />
                <Route path="/products/:id/edit" component={EditProduct} />
                <Route path="/products/:id/delete" component={DeleteProduct} />
                <Route path="/manageProduct" render={(props)=> <ManageProduct {...props}   />} />
                <Route path="/orderList" render={(props)=> <OrderList {...props}  orders={orders} />} />
                <Route path="/thanks" render={(props)=> <ThanksPage {...props}  />} />
                <Route path="/checkout" render={(props)=> <Checkout {...props}  onSubmit={this.handleSubmit}  cart={cart} />} />
                <Route path="/logout" render={(props)=> <MyStoreLogout {...props}   />} />
                <Route path="/login" render={(props)=> <MyStoreLogin {...props}   />} />
                <Route path="/cart" render={(props)=> <Cart {...props}  user={user}   cart={cart} onChange={this.handleChange}/>} />
                <Route  path="/products/:value" render={(props)=> <MyStore {...props} onChange={this.handleChange} cart={cart}/>}/>
                <Route  path="/products/All" component={MyStore}/>
                
               
                <Redirect from="/" to="/products/All" />
                </Switch>
            </div>
        );
    }
}
export default MyStoreMainComp;