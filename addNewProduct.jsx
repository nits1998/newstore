import React, { Component } from "react";
import http from "./httpService";
class AddNewProduct extends Component{
    state={
        product:{category:"",description:"",imgLink:"",name:"",price:""},
    };


    async postData(url,obj){
        let response=await http.post(url, obj);
        console.log(response);
        this.props.history.push("/manageProduct");
    }

    handleChange=(e)=>{
        const {currentTarget:input}=e;
        let s1={...this.state};
        s1.product[input.name]=input.value;
        this.setState(s1);
    };

    handleSubmit=(e)=>{
        e.preventDefault();
        let {product}=this.state;
        console.log(product);
        this.postData(`/products`, product);
       
    };
    
    render(){
        let {prodId,category,description,imgLink,name,price}=this.state.product;
        let arr=["Sunglasses","Watches","Belts","Handbags",
        "Wallets","Formal Shoes","Sport Shoes","Sandals"];
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-1"></div>
                    <div className="col-10">
                            {this.showTextBox("Name",name,"name")}<br/>
                            {this.showTextBox("Description",description,"description")}<br/>
                            {this.showTextBox("Price",price,"price")}<br/>
                            {this.showTextBox("Image",imgLink,"imgLink")}<br/>
                            {this.showDropdown(arr,category,"category",category,"Category")}
                            <br/>
                            <button className="btn btn-sm btn-primary m-1" 
                            onClick={this.handleSubmit}>
                                Add
                            </button>
                    </div>
                    <div className="col-1"></div>
                </div>
            </div>
        );
    }

    showDropdown=(arr,firstVal,name,value,lebel)=>{
        return(
            <React.Fragment>
               <div className="form-group">
               <label className="fw-bold">{lebel}</label>
                <select
                 className="form-select"
                 name={name}
                 onChange={this.handleChange}>
               <option selected disabled value="">
                     {value==="" ? firstVal : value}
                 </option>
                 {arr.map((c1)=>(
                     <option  value={c1}>{c1}</option>
                 ))}
                 </select>
                 
            </div>
            </React.Fragment>
        );
    }

    showTextBox=(label,value,name)=>{
       
        return (
            <React.Fragment>
                <div className="form-group">
                    <label className="fw-bold">{label}</label>
                    <input
                     type="text"
                     className="form-control"
                     id={name}
                     name={name}
                     value={value}
                     //disabled={disabled}
                     onChange={this.handleChange}
                    />
                    
                </div>
                </React.Fragment>
         );
    }
}
export default AddNewProduct;