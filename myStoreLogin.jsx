import React, { Component } from "react";
import http from "./httpService";
import auth from "./authService";
import img1 from "./images/MyStore-sale.jpg";
class MyStoreLogin extends Component{
    state={
        form: {email:"" , password:""},
    };

    handleChange=(e)=>{
        const {currentTarget: input}= e;
        let s1= {...this.state};
        s1.form[input.name]=input.value;
        this.setState(s1);
    }

    async login(url,obj){
        try{
            let response = await http.post(url,obj);
            let {data}= response;
            auth.login(data);
            
            alert("Login Successfull");
            //window.location = "/checkout";
            this.props.history.push("/cart");
        }
        catch (ex) {
            if(ex.response && ex.response.status===400){
                let errors ={}
                errors.email =ex.response.data;
                this.setState({errors:errors});
            }
        }
        
    }

    handleSubmit=(e)=>{
        e.preventDefault();
        this.login("/login",this.state.form);
    }
    
    render(){
        let {email, password}=this.state.form;
        let {errors=null} = this.state;
        return (
            <div className="conatiner">
                <div className="row">
                    <div className="col-1"></div>
                    <div className="col-10 ">
                        <img src={img1} style={{width:100 + "%",height:270}}/>
                        
                    </div>
                    <div className="col-1"></div>
                    </div>
                    <h5 className="text-center">Login</h5>
                    <br/>
                    
                    <div className="row">
                        <div className="col-2"></div>
                        <div className="col-8">
                        <div className="form-group">
                            <label>Email address</label>
                            <input
                            type="text"
                             className="form-control"
                            id="email"
                             name="email"
                            placeholder="Enter Email"
                             value={email}
                            onChange={this.handleChange}
                            />
                            {errors && errors.email && (
                            <span className="text-danger">{errors.email}</span>
                            )}
                        </div>
                    </div>
                    <div className="col-2"></div>
                    </div>

                
                <div className="row">
                    <div className="col-2"></div>
                    <div className="col-8">
                    <div className="form-group">
                    <label>Password</label>
                    <input
                    type="password"
                    className="form-control"
                    id="password"
                    name="password"
                    placeholder="Enter Password"
                    value={password}
                    onChange={this.handleChange}
                    />
                </div>
                    </div>
                    <div className="col-2"></div>
                </div>

                <div className="row">
                    <div className="col-4"></div>
                    <div className="col-4 text-center">
                    <button className="btn btn-sm btn-primary m-2" 
                    onClick={this.handleSubmit}>Login</button>
                    </div>
                    <div className="col-4"></div>
                </div>
                

                
                
            </div>
        );
    }
}
export default MyStoreLogin;