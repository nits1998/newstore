import React, { Component } from "react";
import img1 from "./images/MyStore-sale.jpg";
import http from "./httpService";
import auth from  './authService';
class OrderList extends Component{
    state={
        products:[],
    };

    componentDidUpdate(prevProps, prevState){
        if(prevProps!==this.props) this.getUserData();
    }

    async componentDidMount(){
        let {value}=this.props.match.params;
        console.log(value);
        this.getUserData();
    }
    async getUserData(){
        const user= auth.getUser();
        let {value}=this.props.match.params;
        console.log(value);
        let response={};
        response= await http.get(`/orders/${user.email}`);
        let {data}= response;
        console.log(data);
        
       this.setState({products: data});
    }
    
    render(){
        let {products}=this.state;
        
        return (
            <div className="container">
            <div className="row">
            <div className="col-1"></div>
            <div className="col-10 ">
                <img src={img1} style={{width:100 + "%",height:270}}/>

                <br/>
                <h5>List of Orders</h5>
                <br/>
                <div className="row bg-dark">
                    <div className="col-2 text-white">Name</div>
                    <div className="col-5 text-white">Address</div>
                    <div className="col-2 text-white">City</div>
                    <div className="col-2 text-white">Amount</div>
                    <div className="col-1 text-white">Items</div>
                </div>
                {products.length>0 ? (products.map(m=>(
                    <div className="row border">
                    <div className="col-2">{m.name}</div>
                    <div className="col-5">{m.address}</div>
                    <div className="col-2">{m.city}</div>
                    <div className="col-2">{m.totalPrice}</div>
                    <div className="col-1">{m.items}</div>
                    </div>
                ))) : ""}
                
            </div>
            <div className="col-1"></div>
            </div>
            
            </div>
        );
    }
}
export default OrderList;