import React, { Component } from "react";
import http from "./httpService";
class DeleteProduct extends Component{
    state={};
    async componentDidMount(){
        const {id}=this.props.match.params;
        let response=await http.deleteApi(`/products/${id}`);
        this.props.history.push("/manageProduct");
        console.log(response);
    }
    
    render(){
        return "";
    }
}
export default DeleteProduct;