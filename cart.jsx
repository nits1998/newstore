import React, { Component } from "react";
import img1 from "./images/MyStore-sale.jpg";
import http from "./httpService";
import auth from  './authService';
class Cart extends Component{
    state={
        cart:this.props.cart,
    };

    checkout=()=>{
        const user= auth.getUser();
        let cart=this.props.cart;
        console.log(cart);
        
        (!user) ? this.props.history.push("/login") 
        : user && cart.length===0 ? alert("Please add some products")
        : this.props.history.push("/checkout");
    }

    addOne=(name)=>{
        let cart=this.props.cart;
        let index=cart.findIndex(f=> f.name=== name);
        cart[index].quantity++;
        this.props.onChange(cart);

    }

    minusOne=(name)=>{
        let cart=this.props.cart;
        let index=cart.findIndex(f=> f.name=== name);
    
        cart[index].quantity===1 ? cart.splice(index) : cart[index].quantity--;
        
        this.props.onChange(cart);

    }
    
    render(){
        
        let {cart}=this.state;
        console.log(cart);
        let totalPrice=cart.length>0 ? cart.reduce((acc,cur)=> (+cur.price)*(+cur.quantity)+acc  ,0) : "";
        return (
            <div className="container">
                    <div className="row">
                    <div className="col-1"></div>
                    <div className="col-10 ">
                        <img src={img1} style={{width:100 + "%",height:270}}/>
                        
                    </div>
                    <div className="col-1"></div>
                    </div>

                    <div className="row">
                        <h5 className="text-center">You have {cart.length} Items in Your Cart</h5>
                    </div>
                    <br/>
                    <div className="row">
                    <div className="col-3 fw-bold">Cart Value: Rs.{totalPrice}.00</div>
                    <div className="col-7"></div>
                    <div className="col-2">
                        <button className="btn btn-sm btn-primary m-1" onClick={()=>this.checkout()}>Check Out</button>
                    </div>
                    </div>

                    <div className="row bg-dark text-white">
                        <div className="col-1"></div>
                    <div className="col-5 text-center ">Product details</div>
                    <div className="col-3 text-center">Quantity</div>
                    <div className="col-3 text-center">Price</div>
                    </div>

                    {cart.length>0 ?(cart.map(m=>(
                        <div className="row border">
                        <div className="col-6 ">
                            <div className="row">
                                <div className="col-5">
                                    <img src={m.imgLink} style={{width:150,height:150,padding:10}}/>
                                </div>
                                <div className="col-1"></div>
                                <div className="col-6">
                                    {m.name}<br/>
                                    {m.category}<br/>
                                    {m.description}
                                </div>

                            </div>
                        </div>
                        <div className="col-3 text-center">
                            <button className="btn btn-sm btn-success m-1" 
                            onClick={()=>this.addOne(m.name)}>
                                +
                            </button>

                            {m.quantity}

                            <button className="btn btn-sm btn-warning m-1" 
                            onClick={()=>this.minusOne(m.name)}>
                                -
                            </button>
                        </div>
                        <div className="col-3 text-center">Rs.{m.price}</div>
                        </div>
                    ))) : ("")}
            </div>
        );
    }
}
export default Cart;