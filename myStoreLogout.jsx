import React, { Component } from "react";
import auth from './authService';
class MyStoreLogout extends Component{
    
    componentDidMount(){
        auth.logout();
        //this.props.history.push("/login");
        window.location = "/login";
    }
    
    render(){
        return "";
    }
}
export default MyStoreLogout;