import React, { Component } from "react";
import http from "./httpService";
class EditProduct extends Component{
    state={
        product:[],
    };

    async componentDidMount(){
        this.getUserData();
    }
    async getUserData(){
        const {id}=this.props.match.params;
        let response={};
        response= await http.get(`/product/${id}`);
        let {data}= response;
        console.log(data);
        
       this.setState({product: data[0]});
    }


    async putData(url,obj){
        let response=await http.put(url, obj);
        console.log(response);
        this.props.history.push("/manageProduct");
    }

    handleChange=(e)=>{
        const {currentTarget:input}=e;
        let s1={...this.state};
        s1.product[input.name]=input.value;
        this.setState(s1);
    };

    handleSubmit=(e)=>{
        e.preventDefault();
        const {id}=this.props.match.params;
        let {product}=this.state;
        this.putData(`/products/${id}`, product);
       
    };

    handleDelete=(e)=>{
        e.preventDefault();
        const {id}=this.props.match.params;
        this.props.history.push(`/products/${id}/delete`);
    }
    
    render(){
        let arr=["Sunglasses","Watches","Belts","Handbags",
        "Wallets","Formal Shoes","Sports Shoes","Sandals"];
        let {prodId,category,description,imgLink,name,price}=this.state.product;
        console.log(name);
        return (
            <div className="container-fluid">
                <br/>
                <div className="row">
                    <div className="col-1"></div>
                    <div className="col-10">

                    <h3 className="text-center">Edit Product</h3>
                    <div className="row">
                        <div className="col-6">
                            <div className="bg-dark text-white text-center p-2">
                            <img src={imgLink} style={{width:300,height:200}} />
                            <br/>
                            <h5>{name}</h5><br/>
                            Category: {category}<br/>
                            Price: {price}<br/>

                            </div>
                            
                        </div>
                        <div className="col-6">
                            {this.showTextBox("Name",name,"name")}<br/>
                            {this.showTextBox("Description",description,"description")}<br/>
                            {this.showTextBox("Price",price,"price")}<br/>
                            {this.showTextBox("Image",imgLink,"imgLink")}<br/>
                            {this.showDropdown(arr,category,"category",category,"Category")}
                            <br/>
                            <button className="btn btn-sm btn-primary m-1"
                             onClick={this.handleSubmit}>
                                Save
                            </button>
                            <button className="btn btn-sm btn-secondary m-1" 
                            onClick={this.handleDelete}>
                                Delete
                            </button>

                        </div>
                    </div>
                    
                    
                    </div>
                    <div className="col-1"></div>
                </div>
            </div>
        );
    }

    showDropdown=(arr,firstVal,name,value,lebel)=>{
        return(
            <React.Fragment>
               <div className="form-group">
               <label className="fw-bold">{lebel}</label>
                <select
                 className="form-select"
                 name={name}
                 onChange={this.handleChange}>
               <option selected disabled value="">
                     {value==="" ? firstVal : value}
                 </option>
                 {arr.map((c1)=>(
                     <option  value={c1}>{c1}</option>
                 ))}
                 </select>
                 
            </div>
            </React.Fragment>
        );
    }

    showTextBox=(label,value,name)=>{
       
        return (
            <React.Fragment>
                <div className="form-group">
                    <label className="fw-bold">{label}</label>
                    <input
                     type="text"
                     className="form-control"
                     id={name}
                     name={name}
                     value={value}
                     //disabled={disabled}
                     onChange={this.handleChange}
                    />
                    
                </div>
                </React.Fragment>
         );
    }
}
export default EditProduct;