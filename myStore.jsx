import React, { Component } from "react";
import img1 from "./images/MyStore-sale.jpg";
import http from "./httpService";
class MyStore extends Component{
    state={
        products:[],
        cart:this.props.cart,
    };


    componentDidUpdate(prevProps, prevState){
        if(prevProps!==this.props) this.getUserData();
    }

    async componentDidMount(){
        let {value}=this.props.match.params;
        console.log(value);
        this.getUserData();
    }
    async getUserData(){
        let {value}=this.props.match.params;
        console.log(value);
        let response={};
        response= await http.get(`/products/${value}`);
        let {data}= response;
        console.log(data);
        
       this.setState({products: data});
    }

    goPage=(val)=>{
        //window.location=`/${val}`;
        this.props.history.push(`/products/${val}`);
    }


    addCart=(m)=>{
      
        let s1={...this.state};
       m.quantity=1;
        let z=s1.cart.find(s=>s.prodId==m.prodId);
        let index=s1.cart.findIndex(s=>s.prodId==m.prodId);
        if(!z) s1.cart.push(m);
        if(z) s1.cart.splice(index,1);
        this.setState(s1);
        console.log(s1.cart);
        this.props.onChange(s1.cart);
    }
    
    render(){
        let leftOpt=["All","Sunglasses","Watches","Belts","Handbags",
        "Wallets","Formal Shoes","Sports Shoes","Sandals"];
        let {products,cart}=this.state;
        //console.log(cart);
        return (
            <React.Fragment>
                <div className="container">
                    <div className="row">
                    <div className="col-1"></div>
                    <div className="col-10 ">
                        <img src={img1} style={{width:100 + "%",height:250}}/>
                        
                    </div>
                    <div className="col-1"></div>
                    </div>
                    <div className="row">
                        <div className="col-1"></div>
                        <div className="col-2">
                            {leftOpt.map(m=>(
                                <div className="row border p-1" onClick={()=>this.goPage(m)}>{m}</div>
                            ))}
                        </div>
                        <div className="col-1"></div>
                        <div className="col-8">
                                <div className="row">
                                    {products.map(m=>{
                                        let index=cart.findIndex(s=>s.prodId==m.prodId);
                                        //console.log(index);
                                        let st=index>=0 ? "Remove from Cart" : "Add to Cart" ;
                                        let button=index>=0 ? "btn btn-sm btn-warning m-1" : "btn btn-sm btn-success m-1" ;;

                                        return(
                                        <div className="col-3 m-2 border">
                                            <img src={m.imgLink} style={{height:150, width:150}}/>
                                            <br/>
                                            <h5>{m.name}</h5><br/>
                                            Rs.{m.price}<br/>
                                            <br/>
                                            {m.description}
                                            <br/>
                                            <div className="row"><button className={button} 
                                            onClick={()=>this.addCart(m)}>
                                                {st}
                                            </button>
                                            </div>
                                        </div>
                                        );
                                        })}
                                </div>
                        </div>
                    </div>
                    
                </div>
            </React.Fragment>
        );
    }
}
export default MyStore;