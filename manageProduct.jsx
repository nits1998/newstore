import React, { Component } from "react";
import http from "./httpService";
import {Link} from "react-router-dom";
class ManageProduct extends Component{
    state={
        products:[],
        search:""
    };

    componentDidUpdate(prevProps, prevState){
        if(prevProps!==this.props) this.getUserData();
    }

    async componentDidMount(){
        this.getUserData();
    }
    async getUserData(){
        let {search}=this.state;
        //console.log(search);
        let response={};
        response= await http.get("/products");
        let {data}= response;
        //console.log(data);
        let data2= search !=="" ? data.filter(f=> f.name.includes(search)) : data;
        
       this.setState({products: data2});
    }

    async componentDidUpdate(prevProps,prevState){
        if(prevProps!==this.props) this.getUserData();
    }

    handleChange=async (e)=>{
        const {currentTarget:input}=e;
        let s1={...this.state};
        s1.search=input.value;
        await this.setState(s1);
        console.log(input.value);
        this.getUserData();
        
        //console.log(s1.search);
        
    };

    deleteProduct=(id)=>{
            this.props.history.push(`/products/${id}/delete`);
    }

    addProduct=()=>{
        this.props.history.push(`/addProduct`);
    }
    
    render(){
        let {search,products}=this.state;
        return (
            <div className="container-fluid">
                <br/>
                <div className="row">
                    <div className="col-1"></div>
                    <div className="col-10">

                    <button className="btn btn-sm m-2 btn-success" 
                    onClick={()=>this.addProduct()}
                    >Add a Product</button>
                    <br/>
                    {this.showTextBox(search,"search","Search..")}
                    <br/>
                    <span>Showing products 1-{products.length}</span>
                    <div className="row bg-dark text-white">
                        <div className="col-1 border">#</div>
                        <div className="col-3 border">Tite</div>
                        <div className="col-3 border">Category</div>
                        <div className="col-2 border">Price</div>
                        <div className="col-3 border"></div>
                    </div>
                    {products.map(m=>(
                        <div className="row border" key={m.prodId}>
                        <div className="col-1 border">{m.prodId}</div>
                        <div className="col-3 border">{m.name}</div>
                        <div className="col-3 border">{m.category}</div>
                        <div className="col-2 border">{m.price}</div>
                        <div className="col-3 border">
                            <Link to={`/products/${m.prodId}/edit`}>Edit</Link>
                            <span> </span>
                            <Link to={`/products/${m.prodId}/delete`}>Delete</Link>
                        </div>
                        </div>
                    ))}






                    </div>
                    <div className="col-1"></div>
                </div>
            </div>
        );
    }

    showTextBox=(value,name,placeholder)=>{
       
        return (
            <React.Fragment>
                <div className="form-group">
                   
                    <input
                     type="text"
                     className="form-control"
                     id={name}
                     name={name}
                     value={value}
                     //disabled={disabled}
                     placeholder={placeholder}
                     onChange={this.handleChange}
                    />
                    
                   
                </div>
                </React.Fragment>
         );
    }
}
export default ManageProduct;